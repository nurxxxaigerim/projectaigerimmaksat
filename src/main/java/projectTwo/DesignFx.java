package projectTwo;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;

public class DesignFx extends Application {
    public File file;
    public FileChooser chooser = new FileChooser();
    public Button loadFileButton = new Button("LOAD FILE");
    public BorderPane borderPane = new BorderPane();
    public ArrayList<Question> lisOfQuestion = new ArrayList<Question>();
    public VBox vBox = new VBox();
    public TextArea textArea = new TextArea();
    public RadioButton[] radioButtons = new RadioButton[4];
    public TextField field = new TextField();
    public Label status = new Label();
    public Button check = new Button("Check choosers");
    public HBox hBox = new HBox();
    public HBox hBoxForTextArea = new HBox();
    public Button nextButton = new Button(">>");
    public Button prevButton = new Button("<<");
    public StackPane stackPaneForButton = new StackPane();
    public Text text = new Text("SELECT YOUR QUIZ");
    public Pane paneForText = new Pane();
    public int n = 0;
    public int corAns = 0;

    public Quiz quiz = new Quiz();
    public Question question;

    @Override
    public void start(Stage stage) throws Exception {
        stackPaneForButton.getChildren().add(loadFileButton);
        stackPaneForButton.setStyle("-fx-background-color: #CCFF99");
        text.setX(110);
        text.setY(100);
        text.setFont(Font.font("Verdana", FontWeight.BOLD, FontPosture.REGULAR, 25));
        paneForText.getChildren().add(text);
        paneForText.setStyle("-fx-background-color: #CCFF99");
        paneForText.setPadding(new Insets(20, 20, 20, 20));
        borderPane = new BorderPane();
        borderPane.setTop(paneForText);
        borderPane.setCenter(stackPaneForButton);

        stage.setScene(new Scene(borderPane, 500, 500));
        stage.setTitle("Quiz Game With GUI");
        stage.setResizable(false);
        stage.show();

        loadFileButton.setOnAction(e -> {
            file = chooser.showOpenDialog(stage);
            chooser.setTitle("Opening your quiz");
            if (file.getName() != null) {
                accessFile();
            }
        });
    }

    public void accessFile(){
        try {
            String path = file.getPath();
            quiz = Quiz.loadFromFile(path);
            Collections.shuffle(quiz.getQuestions());
            lisOfQuestion = quiz.getQuestions();

            setLisOfQuestion();
            goingThroughQuestions();
            checking();
        } catch (Exception ex) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText("Invalid Quiz Format!");
            alert.setHeaderText("InvalidQuizFormatException");
            alert.show();
        }
    }

    public void setLisOfQuestion() {
        question = lisOfQuestion.get(n);
        if (question.getClass().equals(Test.class)){
            ArrayList<String> quesList = new ArrayList<>();
            quesList.add((lisOfQuestion.get(n)).getAnswer());
            for (int i = 0; i < 3; i++) {
                quesList.add(((Test)lisOfQuestion.get(n)).getOptionAt(i));
            }

            Collections.shuffle(quesList);

            textArea.setText(lisOfQuestion.get(n).getDescription());

            ToggleGroup group = new ToggleGroup();

            for (int i=0;i<4;i++) {
                radioButtons[i] = new RadioButton(quesList.get(i));
                radioButtons[i].setToggleGroup(group);
            }
            vBox.getChildren().clear();
            vBox.getChildren().addAll(radioButtons);
            vBox.setAlignment(Pos.CENTER);
            BorderPane.setAlignment(vBox, Pos.CENTER);
            borderPane.setCenter(vBox);
        } else {
            String forArea = lisOfQuestion.get(n).getDescription();
            textArea.setText(forArea.replace("{blank}","___"));
            borderPane.setCenter(field);
        }
    }

    public void goingThroughQuestions(){
        nextButton.setOnAction(e ->{
            if (lisOfQuestion.get(n).getClass().equals(FillIn.class)) {
                if (lisOfQuestion.get(n).getAnswer().toUpperCase().equals(field.getText().toUpperCase())) {
                    corAns++;
                }
            }
            else if(lisOfQuestion.get(n).getClass().equals(Test.class)){
                for (int q = 0; q < 4; q++){
                    if(radioButtons[q].isSelected()){
                        if (lisOfQuestion.get(n).getAnswer().equals(radioButtons[q].getText())) {
                            corAns++;
                        }
                    }
                }
            }


            if (lisOfQuestion.size() - 1 == n || lisOfQuestion.size() - 1 < n ) {
                status.setText(" Status : "+(n+1)+"/"+lisOfQuestion.size()+" questions"+"\n End Of Quiz");
            }
            else if (lisOfQuestion.size() - 1 > n){
                status.setText(" Status : "+(n+1)+"/"+lisOfQuestion.size()+" questions");
                n++;
                setLisOfQuestion();
            }
        });

        prevButton.setOnAction(e -> {
            if (n > 0) {
                if (lisOfQuestion.get(n).getClass().equals(FillIn.class)) {
                    if (lisOfQuestion.get(n).getAnswer().toUpperCase().equals(field.getText().toUpperCase())) {
                        corAns++;
                    }
                    field.setText("");
                }
                else if (lisOfQuestion.get(n).getClass().equals(Test.class)) {
                    for (int q = 0; q < 4; q++) {
                        if(radioButtons[q].isSelected()){
                            if (lisOfQuestion.get(n).getAnswer().equals(radioButtons[q].getText())) {
                                corAns++;
                            }
                        }
                    }
                }

                status.setText(" Status : "+(n+1)+"/"+lisOfQuestion.size()+" questions");
                n--;

                setLisOfQuestion();
            }
            else{
                status.setText("Status : "+(n+1)+"/"+lisOfQuestion.size()+" questions"+"\n Start of Quiz");
            }
        });

        hBox.getChildren().add(status);
        hBox.getChildren().add(check);
        hBox.setAlignment(Pos.BOTTOM_CENTER);
        hBoxForTextArea.getChildren().add(textArea);
        hBoxForTextArea.setPadding(new Insets(15,15,15,15));
        hBoxForTextArea.setAlignment(Pos.TOP_CENTER);
        borderPane.setLeft(prevButton);
        borderPane.setBottom(hBox);
        borderPane.setRight(nextButton);
        borderPane.setTop(hBoxForTextArea);

    }
    public void checking(){
        check.setOnMouseClicked(e ->{
            Alert alert1 = new Alert(Alert.AlertType.INFORMATION);
            alert1.setTitle(" Results ");
            alert1.setHeaderText("Correct answers : " + corAns + "/" + lisOfQuestion.size());
            alert1.setContentText("You can try again");
            alert1.show();
        });
    }
}